﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CityInfo.API.Contexts;
using CityInfo.API.Models;
using CityInfo.API.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog;

namespace CityInfo.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CitiesController : ControllerBase
    {
        private readonly ICityInfoRepository _cityInfoRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<CitiesController> _logger;

        public CitiesController(ICityInfoRepository cityInfoRepository, IMapper mapper, ILogger<CitiesController> logger)
        {
            _cityInfoRepository = cityInfoRepository;
            _mapper = mapper;
            _logger = logger;
        }
        
        [HttpGet]
        public IActionResult GetCities()
        {
            var cityEntities = _cityInfoRepository.GetCities();
            // var results = new List<CityWithoutPoI>();
            //
            // foreach (var cityEntity in cityEntities)
            // {
            //     results.Add(new CityWithoutPoI
            //     {
            //         Id = cityEntity.Id,
            //         Description = cityEntity.Description,
            //         Name = cityEntity.Name
            //     });
            // }

            var results = _mapper.Map<IEnumerable<CityWithoutPoIntDto>>(cityEntities);
            return Ok(results);
        }

        [HttpGet("{id}")]
        public IActionResult GetCity(int id, [FromQuery] bool includePoI = false)
        {
            var result = _cityInfoRepository.GetCity(id, includePoI);

            if (result == null)
            {
                return NotFound();
            }

            _logger.LogInformation($"City with ID {id} has been found");
            
            if (includePoI)
            {
                var cityResult = _mapper.Map<CityDto>(result);

                return Ok(cityResult);
            }
            else
            {
                return Ok(_mapper.Map<CityWithoutPoIntDto>(result));
            }
        }
    }
}