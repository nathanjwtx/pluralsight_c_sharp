﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CityInfo.API.Models;
using CityInfo.API.Services;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CityInfo.API.Controllers
{
    [ApiController]
    [Route("api/cities/{cityId}/pointsofinterest")]
    public class PointsOfInterestController : ControllerBase
    {
        private readonly ILogger<PointsOfInterestController> _logger;
        private readonly IMailService _localMailService;
        private readonly ICityInfoRepository _cityInfoRepository;
        private readonly IMapper _mapper;

        public PointsOfInterestController(ILogger<PointsOfInterestController> logger, IMailService localMailService,
            ICityInfoRepository cityInfoRepository, IMapper mapper)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _localMailService = localMailService;
            _cityInfoRepository = cityInfoRepository;
            _mapper = mapper;
        }
        
        [HttpGet]
        public IActionResult GetPointsOfInterest(int cityId)
        {
            try
            {
                if (_cityInfoRepository.CityExists(cityId))
                {
                    var pointsOfInterest = _cityInfoRepository.GetPointsOfInterestForCity(cityId);
                    var results = new List<PointOfInterestDto>();
                    foreach (var pointOfInterest in pointsOfInterest)
                    {
                        results.Add(_mapper.Map<PointOfInterestDto>(pointOfInterest));
                    }
                    
                    // could also do and remove the foreach loop
                    // _mapper.Map<IEnumerable<PointOfInterestDto>>(pointsOfInterest);
                    
                    return Ok(results);
                }
                else
                {
                    _logger.LogInformation($"City with id {cityId} was not found.");
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical($"CRITICAL: Exception while getting points of interest for city with id {cityId}: {ex}");
                return StatusCode(500, "A problem happened while processing this request");
            }
        }

        [HttpGet("{id}", Name = "GetPointOfInterest")]
        public IActionResult GetPointOfInterest(int id, int cityId)
        {
            if (!_cityInfoRepository.CityExists(cityId))
            {
                _logger.LogInformation($"City with id {cityId} was not found.");
                return NotFound();
            }
            
            var pointOfInterestForCity = _cityInfoRepository.GetPointOfInterestForCity(cityId, id);

            if (pointOfInterestForCity == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<PointOfInterestDto>(pointOfInterestForCity));
        }

        [HttpPost]
        public IActionResult CreatePointOfInterest(int cityId, [FromBody] PointOfInterestForPost pointOfInterest)
        {
            if (pointOfInterest.Description == pointOfInterest.Name)
            {
                ModelState.AddModelError(
                    "Description",
                    "The provided description should be different from the name."
                    );
            }
            // this isn't really needed as the ApiController attribute takes care of this automatically unless using
            // custom validation as Model Binding has already occurred
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cityExists = _cityInfoRepository.CityExists(cityId);

            if (!cityExists)
            {
                return NotFound();
            }

            // convert/map from a PointOfInterestForPost model to a PointOfInterest entity so we can save it
            var newPoInt = _mapper.Map<Entities.PointOfInterestEntity>(pointOfInterest);
            
            _cityInfoRepository.AddPointOfInterestForCity(cityId, newPoInt);

            _cityInfoRepository.Save();

            // convert/map from PointOfInterest entity to PointIfInterestDto as returning DTOs is preferable
            var createdPoint = _mapper.Map<Models.PointOfInterestDto>(newPoInt);
            
            return CreatedAtRoute("GetPointOfInterest", new {cityId, id = createdPoint.Id}, 
                createdPoint);
        }

        [HttpPut("{poiId}")]
        public IActionResult UpdatePointOfInterest(int cityId, int poiId, [FromBody] PointOfInterestForPut pointOfInterest)
        {
            if (pointOfInterest.Description == pointOfInterest.Name)
            {
                ModelState.AddModelError(
                    "Description",
                    "The provided description should be different from the name."
                    );
            }
            // this isn't really needed as the ApiController attribute takes care of this automatically unless using
            // custom validation as Model Binding has already occurred
            if (!ModelState.IsValid)
            {
            return BadRequest(ModelState);
            }

            var poiToUpdate = _cityInfoRepository.GetPointOfInterest(poiId);
            if (poiToUpdate == null)
            {
                return NotFound();
            }

            // poiToUpdate is an entity and so changes mapped to it from pointOfInterest will be tracked
            _mapper.Map(pointOfInterest, poiToUpdate);

            _cityInfoRepository.UpdatePointOfInterest(poiToUpdate);
            _cityInfoRepository.Save();
            
            return new NoContentResult();
        }

        [HttpPatch("{poiId}")]
        public IActionResult PartiallyUpdatePointOfInterest(int cityId, int poiId,
            [FromBody] JsonPatchDocument<PointOfInterestForPut> patchDoc)
        {
            if (!_cityInfoRepository.CityExists(cityId))
            {
                return NotFound();
            }

            var pointOfInterestEntity = _cityInfoRepository.GetPointOfInterestForCity(cityId, poiId);

            if (pointOfInterestEntity == null)
            {
                return NotFound();
            }
            
            var pointOfIntToPatch = _mapper.Map<PointOfInterestForPut>(pointOfInterestEntity);
            
            patchDoc.ApplyTo(pointOfIntToPatch, ModelState);

            // at this point the ModelState will be valid as the JsonPatchDocument is valid
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            // need to recheck validation after document has been applied with this new custom validation
            if (pointOfIntToPatch.Description == pointOfIntToPatch.Name)
            {
                ModelState.AddModelError(
                    "Description",
                    "The provided description should be different from the name");
            }

            if (!TryValidateModel(pointOfIntToPatch))
            {
                // return BadRequest("Name can't match Description!");
                return BadRequest(ModelState);
            }

            _mapper.Map(pointOfIntToPatch, pointOfInterestEntity);
            
            _cityInfoRepository.UpdatePointOfInterest(pointOfInterestEntity);
            _cityInfoRepository.Save();

            return new NoContentResult();
        }

        [HttpDelete("{poiId}")]
        public IActionResult DeletePointOfInterest(int poiId, int cityId)
        {
            if (!_cityInfoRepository.PointOfInterestExists(poiId))
            {
                return NotFound();
            }

            var poiToDelete = _cityInfoRepository.GetPointOfInterest(poiId);
            

            if (poiToDelete == null)
            {
                return NotFound("PoI not found.");
            }

            _cityInfoRepository.DeletePointOfInterest(poiToDelete);
            _cityInfoRepository.Save();

            _localMailService.Send("PoI Deleted", $"A PoI, {poiToDelete.Name}, was deleted.");

            return NoContent();
        }
    }
}