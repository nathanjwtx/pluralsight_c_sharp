﻿using System.Collections.Generic;
using CityInfo.API.Models;

namespace CityInfo.API
{
    public class CitiesDataStore
    {
        // readonly as not set which also makes it immutable.
        public static CitiesDataStore Current { get; } = new CitiesDataStore();
        public List<CityDto> Cities { get; set; }

        public CitiesDataStore()
        {
            Cities = new List<CityDto>()
            {
                new CityDto()
                {
                    Id = 1,
                    Name = "Dallas",
                    Description = "The Big D",
                    PointsOfInterest = new List<PointOfInterestDto>()
                    {
                        new PointOfInterestDto()
                        {
                            Id = 1,
                            Name = "Fair Park",
                            Description = "Site of the annual State Fair"
                        },
                        new PointOfInterestDto()
                        {
                            Id = 2,
                            Name = "Dallas Book Repository",
                            Description = "Building where JFK was shot from."
                        }
                    }
                },
                new CityDto()
                {
                    Id = 2,
                    Name = "Wylie",
                    Description = "Arse end of nowhere",
                    PointsOfInterest = new List<PointOfInterestDto>()
                    {
                        new PointOfInterestDto()
                        {
                            Id = 3,
                            Name = "Insync Exotic Big Cat Rescue",
                            Description = "Rescued big cats"
                        },
                        new PointOfInterestDto()
                        {
                            Id = 4,
                            Name = "La Joya",
                            Description = "Best Tex-Mex in town"
                        }
                    }
                },
                new CityDto()
                {
                    Id = 3,
                    Name = "Portsmouth",
                    Description = "Nice little city by the sea",
                    PointsOfInterest = new List<PointOfInterestDto>()
                    {
                        new PointOfInterestDto()
                        {
                            Id = 5,
                            Name = "Round Tower",
                            Description = "Napoleonic Fortifications"
                        },
                        new PointOfInterestDto()
                        {
                            Id = 6,
                            Name = "Portchester Castle",
                            Description = "Castle daing back to Roman times"
                        }
                    }
                },
                new CityDto()
                {
                    Id = 4,
                    Name = "London",
                    Description = "Capital city of England",
                    PointsOfInterest = new List<PointOfInterestDto>()
                }
            };
        }
    }
}