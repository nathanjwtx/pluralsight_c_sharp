﻿using AutoMapper;

namespace CityInfo.API.Profiles
{
    public class PointOfInterestProfile : Profile
    {
        public PointOfInterestProfile()
        {
            CreateMap<Entities.PointOfInterestEntity, Models.PointOfInterestDto>();
            CreateMap<Models.PointOfInterestForPost, Entities.PointOfInterestEntity>();
            // ReverseMap ensures that a mapping goes both ways rather than typing out another line of code but with
            // entity and model reversed
            CreateMap<Models.PointOfInterestForPut, Entities.PointOfInterestEntity>().ReverseMap();
        }
    }
}