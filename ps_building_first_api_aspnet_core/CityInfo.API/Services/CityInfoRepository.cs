﻿using System;
using System.Collections.Generic;
using System.Linq;
using CityInfo.API.Contexts;
using CityInfo.API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Serilog;

namespace CityInfo.API.Services
{
    public class CityInfoRepository : ICityInfoRepository
    {
        private readonly CityInfoContext _context;
        private readonly ILogger<CityInfoRepository> _logger;

        public CityInfoRepository(CityInfoContext context, ILogger<CityInfoRepository> logger)
        {
            _context = context;
            _logger = logger;
        }
        
        public IEnumerable<City> GetCities()
        {
            return _context.Cities.OrderBy(c => c.Name).ToList();
        }

        public City GetCity(int cityId, bool includePoI)
        {
            if (includePoI)
            {
                var result = _context.Cities.Include(c => c.PointsOfInterest)
                    .Where(c => c.Id == cityId).FirstOrDefault();
                return result;
            }

            return _context.Cities.FirstOrDefault(c => c.Id == cityId);
        }

        public IEnumerable<PointOfInterestEntity> GetPointsOfInterestForCity(int cityId)
        {
            return _context.PointsOfInterest.Where(p => p.CityId == cityId).ToList();
        }

        public PointOfInterestEntity GetPointOfInterestForCity(int cityId, int pointOfInterestId)
        {
            return _context.PointsOfInterest.Where(p => p.CityId == cityId && p.Id == pointOfInterestId)
                .FirstOrDefault();
        }

        public bool CityExists(int cityId)
        {
            return _context.Cities.Any(c => c.Id == cityId);
        }

        public void AddPointOfInterestForCity(int cityId, PointOfInterestEntity pointOfInterestEntity)
        {
            var city = _context.Cities.Where(c => c.Id == cityId).FirstOrDefault();
            
            city.PointsOfInterest.Add(pointOfInterestEntity);
        }

        public bool Save()
        {
            _logger.LogInformation($"Changes to save: {_context.SaveChanges().ToString()}");
            return _context.SaveChanges() >= 0;
        }

        public bool PointOfInterestExists(int pointOfInterestId)
        {
            return _context.PointsOfInterest.Any(p => p.Id == pointOfInterestId);
        }

        public PointOfInterestEntity GetPointOfInterest(int pointOfInterestId)
        {
            return _context.PointsOfInterest.Where(p => p.Id == pointOfInterestId).FirstOrDefault();
        }

        public void UpdatePointOfInterest(PointOfInterestEntity pointOfInterestEntity)
        {
            // whilst this is good practice, it isn't required for this implementation as EF Core automatically tracks the entities
        }

        public void DeletePointOfInterest(PointOfInterestEntity pointOfInterestEntity)
        {
            _context.Remove(pointOfInterestEntity);
        }
    }
}