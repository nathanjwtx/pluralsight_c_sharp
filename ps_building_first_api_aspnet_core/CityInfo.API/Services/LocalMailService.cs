﻿using System.Diagnostics;
using Microsoft.Extensions.Configuration;

namespace CityInfo.API.Services
{
    public class LocalMailService : IMailService
    {
        private readonly IConfiguration _config;

        public LocalMailService(IConfiguration config)
        {
            _config = config;
        }
        
        public void Send(string subject, string message)
        {
            var mailSettings = _config.GetSection("mailSettings");
            
            // either method to get the config value works but _config is more succinct
            Debug.WriteLine($"Mail from {mailSettings.GetSection("mailToAddress").Value} to {_config["mailSettings:mailFromAddress"]}, with CloudMailService.");
            Debug.WriteLine($"Subject: {subject}");
            Debug.WriteLine($"Message: {message}");
        }
    }
}