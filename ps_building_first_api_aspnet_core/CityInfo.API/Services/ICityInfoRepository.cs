﻿using System.Collections.Generic;
using System.Linq;
using CityInfo.API.Entities;

namespace CityInfo.API.Services
{
    public interface ICityInfoRepository
    {
        IEnumerable<City> GetCities();
        City GetCity(int cityId, bool includePoI);
        IEnumerable<PointOfInterestEntity> GetPointsOfInterestForCity(int cityId);
        PointOfInterestEntity GetPointOfInterestForCity(int cityId, int pointOfInterestId);
        bool CityExists(int cityId);
        void AddPointOfInterestForCity(int cityId, PointOfInterestEntity pointOfInterestEntity);
        bool Save();
        bool PointOfInterestExists(int pointOfInterestId);
        PointOfInterestEntity GetPointOfInterest(int pointOfInterestId);
        void UpdatePointOfInterest(PointOfInterestEntity pointOfInterest);
        void DeletePointOfInterest(PointOfInterestEntity pointOfInterestEntity);
    }
}