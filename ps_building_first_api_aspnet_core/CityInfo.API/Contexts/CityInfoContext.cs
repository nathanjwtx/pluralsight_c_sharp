﻿using CityInfo.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace CityInfo.API.Contexts
{
    public class CityInfoContext : DbContext
    {
        public CityInfoContext(DbContextOptions<CityInfoContext> options) : base(options)
        {
            // Database.EnsureCreated();
        }
        
        public DbSet<City> Cities { get; set; }
        public DbSet<PointOfInterestEntity> PointsOfInterest { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>()
                .HasData(
                    new City()
                    {
                        Id = 1,
                        Name = "Dallas",
                        Description = "The Big D",
                    },
                    new City()
                    {
                        Id = 2,
                        Name = "Wylie",
                        Description = "Arse end of nowhere",
                    },
                    new City()
                    {
                        Id = 3,
                        Name = "Portsmouth",
                        Description = "Nice little city by the sea",
                    },
                    new City()
                    {
                        Id = 4,
                        Name = "London",
                        Description = "Capital city of England",
                    }
                );
            modelBuilder.Entity<PointOfInterestEntity>()
                .HasData(
                    new PointOfInterestEntity()
                    {
                        Id = 1,
                        CityId = 1,
                        Name = "Fair Park",
                        Description = "Site of the annual State Fair"
                    },
                    new PointOfInterestEntity()
                    {
                        Id = 2,
                        CityId = 1,
                        Name = "Dallas Book Repository",
                        Description = "Building where JFK was shot from."
                    },
                    new PointOfInterestEntity()
                    {
                        Id = 3,
                        CityId = 2,
                        Name = "Insync Exotic Big Cat Rescue",
                        Description = "Rescued big cats."
                    },
                    new PointOfInterestEntity()
                    {
                        Id = 4,
                        CityId = 2,
                        Name = "La Joya",
                        Description = "Best Tex-Mex in town."
                    },
                    new PointOfInterestEntity()
                    {
                        Id = 5,
                        CityId = 3,
                        Name = "Round Tower",
                        Description = "Napoleonic Fortifications."
                    },
                    new PointOfInterestEntity()
                    {
                        Id = 6,
                        CityId = 3,
                        Name = "Portchester Castle",
                        Description = "Castle dating back to Roman times."
                    },
                    new PointOfInterestEntity()
                    {
                        Id = 7,
                        CityId = 4,
                        Name = "Buckingham Palace",
                        Description = "Hereditary home of the British monarchy."
                    }
                );
                base.OnModelCreating(modelBuilder);
        }
    }
}