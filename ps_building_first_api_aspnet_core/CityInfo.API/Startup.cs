﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CityInfo.API.Contexts;
using CityInfo.API.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using Npgsql;
using Serilog;

namespace CityInfo.API
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CityInfoContext>(o =>
            {
                var builder = new NpgsqlConnectionStringBuilder(_configuration.GetConnectionString("PostGres"));
                builder.Password = _configuration["dbpassword"];
                o.UseNpgsql(builder.ConnectionString);
                // o.EnableSensitiveDataLogging();
            });

            services.AddScoped<ICityInfoRepository, CityInfoRepository>();
            // for returning xml
            // services.AddMvc().AddMvcOptions(o =>
            // {
                // o.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
            // });
            // services.AddControllersWithViews().AddNewtonsoftJson();
            services.AddControllers().AddNewtonsoftJson();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "CityInfo", Version = "v1"});
            });
            // use below if json results need or require upper case first letters
            // .AddJsonOptions(o =>
            // {
            //     if (o.SerializerSettings.ContractResolver != null)
            //     {
            //         var castedResolver = o.SerializerSettings.ContractResolver as DefaultContractResolver;
            //         castedResolver.NamingStrategy = null;
            //     }
            // });

            #if DEBUG
            services.AddTransient<IMailService, LocalMailService>();
            #else
            services.AddTransient<IMailService, CloudMailService>();
            #endif

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSerilogRequestLogging();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CityInfo v1"));
            }
            else
            {
                app.UseExceptionHandler();
            }

            app.UseRouting();
            app.UseStatusCodePages();

            app.UseEndpoints(ep =>
            {
                ep.MapControllers();
            });
            // app.UseMvc();
        }
    }
}
